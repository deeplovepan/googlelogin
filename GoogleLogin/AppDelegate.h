//
//  AppDelegate.h
//  GoogleLogin
//
//  Created by Peter Pan on 3/12/15.
//  Copyright (c) 2015 Peter Pan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

