//
//  ViewController.m
//  GoogleLogin
//
//  Created by Peter Pan on 3/12/15.
//  Copyright (c) 2015 Peter Pan. All rights reserved.
//

#import "ViewController.h"
#import "GoogleLoginViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loginButPressed:(id)sender {
    
    GoogleLoginViewController *controller = [[GoogleLoginViewController alloc] init];
    [self.navigationController pushViewController:controller animated:YES];
}


@end
